sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin go ruby-irb rubygems rubygem-bigdecimal rubygem-rake rubygem-i18n rubygem-bundler git svn ruby -y
sudo dnf -y install ruby-devel libpcap-devel
sudo gem install rake
sudo dnf -y install postgresql-server postgresql-devel
sudo gem install pg
wget http://downloads.metasploit.com/data/releases/framework-latest.tar.bz2
tar -jxf framework-latest.tar.bz2
sudo mkdir -p /opt/metasploit4
sudo cp -a msf*/ /opt/metasploit4/msf
sudo chown root:root -R /opt/metasploit4/msf
sudo ln -sf /opt/metasploit4/msf/msf* /usr/local/bin/
sudo gem install haiti-hash
sudo gem install wpscan
sudo systemctl enable --now docker
sudo docker pull rustscan/rustscan:latest
usermod -aG docker $USER
sudo dnf install nmap binwalk ffuf wireshark john hydra netcat aircrack-ng hashcat hexedit nikto remmina ftp macchanger -y 
wget -c https://github.com/danielmiessler/SecLists/archive/master.zip -O SecList.zip \
  && unzip SecList.zip \
  && rm -f SecList.zip

firefox https://portswigger.net/burp/releases#community
